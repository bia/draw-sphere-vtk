package plugins.lagache.spherevtk;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import icy.plugin.abstract_.Plugin;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarColor;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarDoubleArrayNative;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.kernel.roi.roi2d.ROI2DPoint;

// Colocalisation with Ripley function K
// Significant 

public class DrawSpheresVTK extends Plugin implements Block {

	VarSequence input_sequence = new VarSequence("Input sequence", null);
	VarDoubleArrayNative x= new VarDoubleArrayNative("x", null);
	VarDoubleArrayNative y= new VarDoubleArrayNative("y", null);
	VarDoubleArrayNative z= new VarDoubleArrayNative("z", null);
	VarString name = new VarString("Name of the overlay", "detections");
	VarColor color = new VarColor("Color",  Color.blue);
	VarDouble size = new VarDouble("Size", 1.0);	
		@Override
	public void declareInput(VarList inputMap) {

		inputMap.add("Input Sequence", input_sequence);
		inputMap.add("x",x);
		inputMap.add("y",y);
		inputMap.add("z",z);
		inputMap.add("Size",size);		
		inputMap.add("Color",color);		
		inputMap.add("Name of the overlay",name);
	}

	@Override
	public void declareOutput(VarList outputMap) {		
	}

		@Override
	public void run() {
			
		 // Add the cross overlay, it becomes active after being added.				
        input_sequence.getValue().addOverlay(new SphereVTK(x.getValue(),y.getValue(),z.getValue(),size.getValue(),color.getValue(),name.getValue()));
		}
		
		}
		
	

	