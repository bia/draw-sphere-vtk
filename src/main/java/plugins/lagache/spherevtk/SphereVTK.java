/*
 * Copyright 2010, 2011 Institut Pasteur.
 * 
 * This file is part of ICY.
 * 
 * ICY is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ICY is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ICY. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.lagache.spherevtk;

import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.painter.VtkPainter;
import icy.sequence.Sequence;
import icy.vtk.VtkUtil;

import java.awt.Color;
import java.awt.Graphics2D;

import plugins.kernel.canvas.VtkCanvas;
import vtk.vtkActor;
import vtk.vtkFloatArray;
import vtk.vtkGlyph3D;
import vtk.vtkLookupTable;
import vtk.vtkMolecule;
import vtk.vtkMoleculeMapper;
import vtk.vtkPoints;
import vtk.vtkPolyDataMapper;
import vtk.vtkProp;
import vtk.vtkSphereSource;
import vtk.vtkUnsignedShortArray;
import vtk.vtkUnstructuredGrid;

/**
 * @author stephane
 */
public class SphereVTK extends Overlay implements VtkPainter
{
    double[] x, y, z;
    double size;
    Color color;

    private vtkActor[] sphere_ens;

    public SphereVTK(double[] x, double[] y, double[] z, double size, Color color, String name)
    {
        super(name);

        this.x = x;
        this.y = y;
        this.z = z;
        this.size = size;
        this.color = color;

        init();
    }

    private void init()
    {
        // create points
        final vtkPoints points = new vtkPoints();
        // setup scales
        final vtkFloatArray scales = new vtkFloatArray();
        scales.SetName("scales");
        // setup color label
        final vtkFloatArray col = new vtkFloatArray();
        col.SetName("col");

        // setup lookupTable and add some colors
        final vtkLookupTable colors = new vtkLookupTable();
        colors.SetNumberOfTableValues(1);
        // index, R, G, B, A
        colors.SetTableValue(0, color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, 1.0);

        for (int i = 0; i < x.length; i++)
        {
            points.InsertNextPoint(x[i], y[i], z[i]);
            scales.InsertNextValue(1);
            col.InsertNextValue(0); // first color
        }

        // grid structured to append center, radius and color label
        final vtkUnstructuredGrid grid = new vtkUnstructuredGrid();
        
        grid.SetPoints(points);
        grid.GetPointData().AddArray(scales);
        grid.GetPointData().SetActiveScalars("scales"); // !!!to set radius first
        grid.GetPointData().AddArray(col);
        
        points.Delete();
        scales.Delete();
        col.Delete();

        // create anything you want here, we will use a sphere for the demo
        final vtkSphereSource sphere = new vtkSphereSource();

        sphere.SetRadius(size);
        sphere.SetThetaResolution(6);
        sphere.SetPhiResolution(6);

        // object to group sphere and grid and keep smooth interaction
        final  vtkGlyph3D glyph3D = new vtkGlyph3D();
        
        glyph3D.SetInputData(grid);
        glyph3D.SetSourceConnection(sphere.GetOutputPort());

        // create a mapper and actor
        final vtkPolyDataMapper mapper = new vtkPolyDataMapper();
        mapper.SetInputConnection(glyph3D.GetOutputPort());

        mapper.SetScalarModeToUsePointFieldData(); // without, color are displayed regarding radius and not color label
        // mapper.SetScalarRange(0, 3); // to scale color label (without, col should be between 0 and 1)
        mapper.SelectColorArray("col"); // !!!to set color (nevertheless you will have nothing)
        mapper.SetLookupTable(colors);

        final vtkActor actor = new vtkActor();
        actor.SetMapper(mapper);

        sphere_ens = new vtkActor[1];
        sphere_ens[0] = actor;
    }

    // init vtk objects
    private void init_2()
    {
        final double[] pts = new double[x.length * 3];

        // convert to single array
        for (int i = 0; i < x.length; i++)
        {
            pts[(i * 3) + 0] = x[i];
            pts[(i * 3) + 1] = y[i];
            pts[(i * 3) + 2] = z[i];
        }

        final vtkPoints vtkPts = VtkUtil.getPoints(pts);

        sphere_ens = new vtkActor[1];
        final vtkMolecule mol = new vtkMolecule();

        mol.SetPoints(vtkPts);
        vtkPts.Delete();

        final vtkUnsignedShortArray atomicNums = (vtkUnsignedShortArray) mol.GetVertexData().GetScalars();

        // set atomic num
        for (int i = 0; i < x.length; i++)
            atomicNums.InsertValue(i, 0);

        // mapper
        final vtkMoleculeMapper molMapper = new vtkMoleculeMapper();

        molMapper.SetInputData(mol);
        molMapper.UseFastSettings();

        // molMapper.SetRenderAtoms(true);
        // molMapper.SetRenderBonds(false);

        // actor
        final vtkActor molActor = new vtkActor();
        molActor.SetMapper(molMapper);

        sphere_ens[0] = molActor;
    }

    // init vtk objects
    private void init_old()
    {
        sphere_ens = new vtkActor[x.length];
        for (int i = 0; i < x.length; i++)
        {
            // source
            final vtkSphereSource sphere = new vtkSphereSource();
            sphere.SetRadius(size);
            sphere.SetThetaResolution(6);
            sphere.SetPhiResolution(6);

            sphere.SetCenter(x[i], y[i], z[i]);

            // mapper
            final vtkPolyDataMapper map = new vtkPolyDataMapper();
            map.SetInputConnection(sphere.GetOutputPort());

            // actor
            vtkActor aSphere = new vtkActor();
            aSphere.SetMapper(map);
            // double total = color.getRed()+ color.getGreen()+ color.getBlue();
            double red = color.getRed() / 255.;
            double green = color.getGreen() / 255.;
            double blue = color.getBlue() / 255.;

            aSphere.GetProperty().SetColor(red, green, blue);
            sphere_ens[i] = aSphere;
        }
    }
    
    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        super.paint(g, sequence, canvas);

        if (canvas instanceof VtkCanvas)
        {
            // disable picking by using this property
            ((VtkCanvas) canvas).getPicker().PickFromListOn();
        }
    }

    @Override
    public vtkProp[] getProps()
    {
        return sphere_ens;
    }
}